# ActionLogBundle

### Required Parameters

* aws_key: <AWS_KEY>
* aws_secret: <AWS_SECRET_KEY>
* aws_region: eu-west-1

### Basic Use

`$this->get('alpha.action_log')->logAction($accountId, $action, array('attribute' => $value));`

### Use with API

When using the API trait `CommonParentResourceMethods` you can implement this method:

`protected function log($entity, $crud)`

Which will pass in the entity being acted on and crud method taking place. Remember you can only log actions where the entity has an _accountId_ property.

If you want to log entities then the entity must also implement a getter `public function getLog()` which returns an array of attributes to log with the action.

#### Example Usage

    protected function log($entity, $crud) {
        $accountId = $entity->getAccountId();
        $data = $entity->getLog();

        switch($crud) {
            case 'create':
                $action = 'createdAlpha';
                break;
            default:
                return;
                break;
        }

        $this->get('alpha.action_log')->logAction($accountId, $action, $data);
    }